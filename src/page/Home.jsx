import React, { useState } from 'react'
import { Button, TextField } from '@material-ui/core'
import { BallTriangle, ThreeCircles } from 'react-loader-spinner'
import { DropzoneArea } from 'material-ui-dropzone'
import Papa from 'papaparse'
import JSZip from 'jszip'
import axios from 'axios'
import '../App.css'
import logo from '../media/logo.png'


function Home() {
    const [files, setFiles] = useState([])
    const [annotations, setAnnotations] = useState([])
    const [annotationsCTA, setAnnotationsCTA] = useState([])
    const [foodOnAnnotations, setFoodOnAnnotations] = useState([])
    const [foodOnAnnotationsCTA, setFoodOnAnnotationsCTA] = useState([])
    const [loader, setLoader] = useState(false)
  
    const handleFiles = (uploadedFiles) => {
        setFiles(uploadedFiles)
    }

    const handleAnnotate = async () => {

        const newAnnotations = []
        const newAnnotationsCTA = []

        for (const file of files) {
            const fileData = await file.text()
            const parsedData = Papa.parse(fileData, { header: true })
            let lineNumber = 1
            for (const row of parsedData.data) {
                for (let i = 0; i < parsedData.meta.fields.length; i++) {
                    const entityURI = await getEntityURI(row[parsedData.meta.fields[i]])
                    newAnnotations.push({
                        file: file.name,
                        col: i,
                        row: lineNumber,
                        URI: entityURI,
                    })
                }
                lineNumber++;
            }
            for (let i = 0; i < parsedData.meta.fields.length; i++) {
                newAnnotationsCTA.push({
                    file: file.name,
                    col: i,
                    URI: await getEntityURI(parsedData.meta.fields[i]),
                })
            }
        }

        newAnnotations.sort((a, b) => {
            if (a.file < b.file) {
                return -1
            } else if (a.file > b.file) {
                return 1
            } else {
                return a.col - b.col
            }
        })

        setAnnotations(newAnnotations)
        setAnnotationsCTA(newAnnotationsCTA)
    }

    const handleAnnotateFoodOn = async () => {

        const newAnnotations = []
        const newAnnotationsCTA = []

        for (const file of files) {
            const fileData = await file.text()
            const parsedData = Papa.parse(fileData, { header: true })
            let lineNumber = 1
            for (const row of parsedData.data) {
                for (let i = 0; i < parsedData.meta.fields.length; i++) {
                    const entityURI = await getFoodOnEntityURI(row[parsedData.meta.fields[i]])
                    newAnnotations.push({
                        file: file.name,
                        col: i,
                        row: lineNumber,
                        URI: entityURI,
                    })
                }
                lineNumber++;
            }

            for (let i = 0; i < parsedData.meta.fields.length; i++) {
                newAnnotationsCTA.push({
                    file: file.name,
                    col: i,
                    URI: await getFoodOnEntityURI(parsedData.meta.fields[i]),
                })
            }
        }
    
        newAnnotations.sort((a, b) => {
            if (a.file < b.file) {
                return -1
            } else if (a.file > b.file) {
                return 1
            } else {
                return a.col - b.col
            }
        })
    
        setFoodOnAnnotations(newAnnotations)
        setFoodOnAnnotationsCTA(newAnnotationsCTA)
    }
    
    const getEntityURI = async (entityName) => {
        if (!entityName || typeof entityName !== 'string') {
            return 'NIL'
        }
    
        setLoader(true)

        try {
            const response = await axios.get('/w/api.php', {
                params: {
                    action: 'wbsearchentities',
                    format: 'json',
                    language: 'en',
                    search: entityName,
                },
            })
    
            const entityURI = response.data.search[0]?.concepturi
            console.log(response.data.search[0]?.concepturi)
            setLoader(false)
            return entityURI ? entityURI : 'NIL'
        } catch (error) {
            console.error(error)
            return null
        }
    }

    const getFoodOnEntityURI = async (entityName) => {
        if (!entityName || typeof entityName !== 'string') {
            return 'NIL'
        }
    
        setLoader(true)
    
        try {
            const response = await axios.get(`https://www.ebi.ac.uk/ols/api/search?q=${encodeURIComponent(entityName)}&ontology=foodon`)
    
            const entityURI = response.data.response.docs[0].iri;
            setLoader(false)
            return entityURI
        } catch (error) {
            console.error(error)
            return null
        }
    } 
  
    // const downloadAnnotations = () => {
    //     const csvData = annotations.length > 0 ? Papa.unparse(annotations) : Papa.unparse(foodOnAnnotations)
    //     const blob = new Blob([csvData], { type: 'text/csv;charset=utf-8;' })
    //     const url = URL.createObjectURL(blob)
    //     const link = document.createElement('a')
    //     link.href = url
    //     link.setAttribute('download', 'annotatedFile.csv')
    //     document.body.appendChild(link)
    //     link.click()
    //     document.body.removeChild(link)
    // }

    const downloadAnnotations = () => {

        if (annotations.length > 0) {

            const zip = new JSZip()
            const ceaData = Papa.unparse(annotations)
            const ctaData = Papa.unparse(annotationsCTA)
        
            // Ajouter les fichiers à l'archive
            zip.file('cea.csv', ceaData)
            zip.file('cta.csv', ctaData)
        
            // Générer l'archive
            zip.generateAsync({ type: 'blob' }).then((blob) => {
              // Télécharger l'archive
              const url = URL.createObjectURL(blob)
              const link = document.createElement('a')
              link.href = url;
              link.setAttribute('download', 'wikidataFiles.zip')
              document.body.appendChild(link)
              link.click()
              document.body.removeChild(link)
            })
        } 
        
        if (foodOnAnnotations.length > 0) {

            const zip = new JSZip()
            const ceaData = Papa.unparse(foodOnAnnotations)
            const ctaData = Papa.unparse(foodOnAnnotationsCTA)
        
            // Ajouter les fichiers à l'archive
            zip.file('cea.csv', ceaData)
            zip.file('cta.csv', ctaData)
        
            // Générer l'archive
            zip.generateAsync({ type: 'blob' }).then((blob) => {
              // Télécharger l'archive
              const url = URL.createObjectURL(blob)
              const link = document.createElement('a')
              link.href = url;
              link.setAttribute('download', 'foodonFiles.zip')
              document.body.appendChild(link)
              link.click()
              document.body.removeChild(link)
            })
        }
    }

    return (
        <div style={{ paddingTop: 30, paddingLeft: 90, paddingRight: 90 }}>

            <img src={logo} alt="" height={90} />

            <p style={{ color: '#fff', fontSize: 23, fontWeight: 'bold', textTransform: 'uppercase', marginBottom: 30, marginTop: 0 }}>
                Jugalux File-Annotator
            </p>
            {/* <p style={{ color: '#fff', marginBottom: 30 }}>
                Here you can annotate your source files and get their CEA and CTA files with URIs of wikidata or FoodOn. 
                Just import all your cleaned files (maximum 250 files), click on the Annotate button 
                and wait. Then you can download the generated files
            </p> */}
            <DropzoneArea
                acceptedFiles={['text/csv']}
                dropzoneText="Drag and drop or click here to select your CSV cleaned files"
                onChange={handleFiles}
                filesLimit={250}
                classes={{
                    root: 'MuiDropzoneArea-root',
                    active: 'MuiDropzoneArea-active',
                    text: 'MuiDropzoneArea-text',
                    textSecondary: 'MuiTypography-colorTextSecondary',
                    icon: 'MuiDropzoneArea-icon'
                }}
            />
            <br />
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button 
                    variant="contained" 
                    color="#fff" 
                    style={{ paddingLeft: 50, paddingRight: 50, backgroundColor: '#fff', color: 'blueviolet', fontSize: 18, fontWeight: 'bold', textTransform: 'none' }} 
                    onClick={handleAnnotate}
                >
                    Annotate with Wikidata
                </Button>
                <Button
                    variant="contained"
                    color="#fff"
                    style={{ paddingLeft: 50, paddingRight: 50, backgroundColor: '#fff', color: 'blueviolet', fontSize: 18, fontWeight: 'bold', textTransform: 'none', marginLeft: 20 }}
                    onClick={handleAnnotateFoodOn}
                >
                    Annotate with FoodOn
                </Button>
            </div>
            <br />
            <br />
            {
                loader ? 
                        <div style={{ marginTop: 50 }}>
                            <div style={{ marginLeft: '45%' }}>
                                <BallTriangle
                                    color="#fff"
                                />
                            </div>
                            <p style={{ color: '#fff', marginBottom: 30, textAlign: 'center' }}>
                                Wait a moment please...
                            </p>
                        </div>
                    :
                    
                        <>
                            {
                                annotations.length > 0 && (
                                    <>
                                        <TextField 
                                            label="Wikidata CEA Annotations" 
                                            multiline 
                                            rows={10} 
                                            variant="outlined" 
                                            value={JSON.stringify(annotations)} 
                                            style={{ width: '95%' }} 
                                            InputProps={{ style: { borderColor: '#fff', color: '#fff' } }}
                                            InputLabelProps={{ style: { color: '#fff', fontSize: 17, fontWeight: 'bold' } }}
                                        />
                                        <TextField 
                                            label="Wikidata CTA Annotations" 
                                            multiline 
                                            rows={5} 
                                            variant="outlined" 
                                            value={JSON.stringify(annotationsCTA)} 
                                            style={{ width: '95%' }} 
                                            InputProps={{ style: { borderColor: '#fff', color: '#fff' } }}
                                            InputLabelProps={{ style: { color: '#fff', fontSize: 17, fontWeight: 'bold' } }}
                                        />
                                    </>
                                )
                            }
                            {
                                foodOnAnnotations.length > 0 && (
                                    <>
                                        <TextField
                                            label="FoodOn CEA Annotations"
                                            multiline
                                            rows={10}
                                            variant="outlined"
                                            value={JSON.stringify(foodOnAnnotations)}
                                            style={{ width: '95%', marginTop: 20 }}
                                            InputProps={{ style: { borderColor: '#fff', color: '#fff' } }}
                                            InputLabelProps={{ style: { color: '#fff', fontSize: 17, fontWeight: 'bold' } }}
                                        />
                                        <TextField
                                            label="FoodOn CTA Annotations"
                                            multiline
                                            rows={5}
                                            variant="outlined"
                                            value={JSON.stringify(foodOnAnnotationsCTA)}
                                            style={{ width: '95%', marginTop: 20 }}
                                            InputProps={{ style: { borderColor: '#fff', color: '#fff' } }}
                                            InputLabelProps={{ style: { color: '#fff', fontSize: 17, fontWeight: 'bold' } }}
                                        />
                                    </>
                                )
                            }
                            <br />
                            <br />
                            {
                                (annotations.length > 0 || foodOnAnnotations.length > 0) && (
                                    <Button 
                                        variant="contained" 
                                        color="blueviolet" 
                                        onClick={downloadAnnotations}
                                        style={{ backgroundColor: '#ca5cdd', color: '#fff', textTransform: 'none', fontSize: 17, marginBottom: 50 }}
                                    >
                                        Download Annotations
                                    </Button>
                                )
                            }
                            
                        </>
            }
        </div>
    )
}

export default Home