import './App.css';
import Home from './page/Home';

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;


// const handleAnnotate = async () => {

//   const newAnnotations = []

//   for (const file of files) {
//       const fileData = await file.text()
//       const parsedData = Papa.parse(fileData, { header: true })

//       for (const row of parsedData.data) {
//           const entityURI = await getEntityURI(Object.values(row)[0])
//           newAnnotations.push({
//               FileName: file.name,
//               column: row.Column,
//               row: row.Row,
//               URI: entityURI,
//           })
//       }
//   }
//   setAnnotations(newAnnotations)
// }